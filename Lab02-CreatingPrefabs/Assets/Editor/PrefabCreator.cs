﻿    using UnityEngine;
using System.Collections;
using UnityEditor;

    /**
    @author Seth Timmons
    @date 09/07/2016
    PrefabCreator class is used to allow the user to
    create a new prefab using a menu item
    */
public class PrefabCreator : MonoBehaviour {
    /// <summary>
    /// CreatePrefab is used to find selected objects
    /// and create prefabs out of them in a certain
    /// assetPath
    /// </summary>
	[MenuItem("Project Tools/Create Prefab")]
    public static void CreatePrefab()
    {
        GameObject[] selectedObjects = Selection.gameObjects;
        foreach(GameObject go in selectedObjects)
        {
            string name = go.name;
            string assetPath = "Assets/" + name + ".prefab";

            if(AssetDatabase.LoadAssetAtPath(assetPath, typeof(GameObject)))
            {
                if (EditorUtility.DisplayDialog("Caution", "Prefab " + name + " already exists. Do you want to overwrite?", "Yes", "No"))
                {
                    createNew(go, assetPath);
                }
            }
            else
            {
                createNew(go, assetPath);
            }
            //Debug.Log("Name: " + go.name + " Path: " + assetPath);
        }
    }
    /// <summary>
    /// createNew is used to transform a chosen
    /// object into a prefab.
    /// </summary>
    /// <param name="obj"></param>
    /// The object that has been selected 
    /// <param name="location"></param>
    /// Where the new prefab will be placed
    public static void createNew(GameObject obj, string location)
    {
        Object prefab = PrefabUtility.CreateEmptyPrefab(location);
        PrefabUtility.ReplacePrefab(obj, prefab);
        AssetDatabase.Refresh();

        DestroyImmediate(obj);
        GameObject clone = PrefabUtility.InstantiatePrefab(prefab) as GameObject;
    }
}
